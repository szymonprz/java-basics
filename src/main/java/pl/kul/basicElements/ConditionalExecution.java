package pl.kul.basicElements;

public class ConditionalExecution {

    public static void main(String[] args) {

        String someVariable = "hey";

        if(someVariable.equals("hello")){

        }else if(someVariable.equals("hey")){

        }else{

        }


        int variable = 10;

        {
            //block of execution
//            int variable = 5;
        }

        int x = 10;

        while(x > 5){
            //do something
            x--;
        }

        do{
            //do something
            x--;
        }while(x>5);


        for (int i=0; i< 10; i++){
            System.out.println(i);
        }

        for(int i=0, j=5; i<10 && j< 20; i*=2, j*=3){
            System.out.println(i + " " +j);
        }


        switch(x){
            case 5:
                System.out.println("");
            case 10:
                break;
            default  : {


            }
        }


        String[] words = {"hello", "world", "asdfasdf"};

        for (String word : words) {
            System.out.println(word);
        }
    }
}
