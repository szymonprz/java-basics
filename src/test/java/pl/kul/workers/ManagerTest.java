package pl.kul.workers;

import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ManagerTest {


    @Test
    public void shouldIncludeBasicBonusInSalaryDetails(){
        // given
        Employee employee = new Manager(5);

        // when
        BigDecimal salary = employee.getSalary();

        // then
        Assertions.assertThat(salary).isEqualTo("5500");
    }


    @Test
    public void shouldIncludeInitialBonusInSalaryDetails(){
        // given
        Employee employee = new Manager(5, new BigDecimal(1000));

        // when
        BigDecimal salary = employee.getSalary();

        // then
        Assertions.assertThat(salary).isEqualTo("6000");
    }

}