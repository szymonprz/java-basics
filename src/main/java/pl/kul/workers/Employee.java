package pl.kul.workers;

import java.math.BigDecimal;

public class Employee {

    private int yearsOfExperience;

    Employee(int yearsOfExperience){
        this.yearsOfExperience = yearsOfExperience;
    }

    public BigDecimal getSalary(){
        return new BigDecimal(1000).multiply(BigDecimal.valueOf(yearsOfExperience));
    }


}
