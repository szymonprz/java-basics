package pl.kul.workers;

import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class ProgrammerTest {

    @Test
    public void shouldHaveSalaryEqualToExpYearsMultipliedByThousand(){
        // given
        Employee employee = new Programmer(5);

        // when
        BigDecimal salary = employee.getSalary();

        // then
        assertThat(salary).isEqualTo("5000");
    }

}