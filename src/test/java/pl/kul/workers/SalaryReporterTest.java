package pl.kul.workers;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SalaryReporterTest {

    private SalaryReporter salaryReporter = new SalaryReporter();


    @Test
    public void shouldIncludeProgrammersAndManagersSalaryInSum(){
        // given
        Employee programmer = new Programmer(5);
        Employee manager = new Manager(5);
        List<Employee> employees = Arrays.asList(programmer, manager);

        // when
        BigDecimal sumOfSalaries = salaryReporter.countSumOfSalaries(employees);

        // then
        assertThat(sumOfSalaries).isEqualTo("10500");
    }

}