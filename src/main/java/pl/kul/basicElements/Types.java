package pl.kul.basicElements;

public class Types {


    static int staticVariable = 5;

    public static void main(String[] args) {

        int intExample = 4;
        short shortExample = 2;
        long longExample = 8;
        byte byteExample = 1;

        float floatExample = 1.0f;
        double doubleExample = 1.0;

        char charExample =  'a';
        boolean booleanExample = false;

        final int constVariable = 4;
//        constVariable = 8;


        //przykład static variable




        double x = 9.997;
        int nx = (int) x; //x = 9;


        Size someSize = Size.MEDIUM;




        String first = "word";
        String second = "Hello";
        String message = second + " " + first;
        String helloAgain = message.substring(0, 5);
        boolean isSame = second.equals(helloAgain);


        int[] array = new int[3];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;

        int array2[] = new int[4];

        int array3[] = {2, 3, 4};

        int twoDimension[][] = new int[5][5];
        twoDimension[0][0] = 1;


        int twoDimension2[][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};




    }
}

enum Size {
    SMALL, MEDIUM, LARGE, XLARGE
}
