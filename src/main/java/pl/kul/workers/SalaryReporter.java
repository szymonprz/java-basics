package pl.kul.workers;

import java.math.BigDecimal;
import java.util.List;

public class SalaryReporter {

    public BigDecimal countSumOfSalaries(List<Employee> employees){
        BigDecimal sum = BigDecimal.ZERO;
        for (Employee employee : employees) {
            sum = sum.add(employee.getSalary());
        }
        return sum;
    }
}
