package pl.kul.workers;

import java.math.BigDecimal;

public class Manager extends Employee {


    private BigDecimal bonus = new BigDecimal(500);

    public Manager(int yearsOfExperience) {
        super(yearsOfExperience);
    }

    public Manager(int yearsOfExperience, BigDecimal initialBonus){
        super(yearsOfExperience);
        bonus = initialBonus;
    }

    @Override
    public BigDecimal getSalary() {
        BigDecimal baseSalary = super.getSalary();
        return baseSalary.add(bonus);
    }



}
